#ifndef CHAR_LOADER_H
#define CHAR_LOADER_H

#include "3DShapes.h"

void loadCharacterFromFile(const char* characterFile, Shape *&torso,
	Shape *&head, Shape *&arm, Shape *&leg, float& distanceBetweenParts);

#endif