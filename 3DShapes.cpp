#ifdef __APPLE__  // include Mac OS X verions of headers
#  include <OpenGL/OpenGL.h>
#  include <GLUT/glut.h>
#else // non-Mac OS X operating systems
#  include <GL/glew.h>
#  include <GL/freeglut.h>
#  include <GL/freeglut_ext.h>
#endif  // __APPLE__

#include "3DShapes.h"

enum {CUBE, SPHERE, CONE, CYLINDER};

void Cube::draw(void) {
    glRotatef(xRotation, 1.0, 0.0, 0.0);
    glRotatef(yRotation, 0.0, 1.0, 0.0);
    glColor3f(primaryColor.x(), primaryColor.y(), primaryColor.z());
    glutSolidCube(size);
    glColor3f(secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
    glutWireCube(size);
}

void Sphere::draw(void) {
    glRotatef(xRotation, 1.0, 0.0, 0.0);
    glRotatef(yRotation, 0.0, 1.0, 0.0);
    glColor3f(primaryColor.x(), primaryColor.y(), primaryColor.z());
    glutSolidSphere(radius, 20, 20);
    glColor3f(secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
    glutWireSphere(radius, 20, 20);
}

void Cone::draw(void) {
    GLUquadricObj *cone, *base;
    cone = gluNewQuadric();
    base = gluNewQuadric();
    glRotatef(xRotation, 1.0, 0.0, 0.0);
    glRotatef(yRotation, 0.0, 1.0, 0.0);
    // Make cone centered at origin.
    glTranslatef(0, 0, -height() / 2);
    glColor3f(primaryColor.x(), primaryColor.y(), primaryColor.z());
    gluCylinder(cone, radius, 0.0, height(), 20, 20);
    glColor3f(secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
    gluDisk(base, 0.0, radius, 20, 1);
    gluDeleteQuadric(cone);
    gluDeleteQuadric(base);
}

void Cylinder::draw(void) {
    GLUquadricObj *cone, *base;
    cone = gluNewQuadric();
    base = gluNewQuadric();
    glRotatef(xRotation, 1.0, 0.0, 0.0);
    glRotatef(yRotation, 0.0, 1.0, 0.0);
    // Make cylinder centered at origin.
    glTranslatef(0, 0, -height() / 2);
    glColor3f(primaryColor.x(), primaryColor.y(), primaryColor.z());
    gluCylinder(cone, radius, radius, height(), 20, 20);
    glColor3f(secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
    gluDisk(base, 0.0, radius, 20, 1);
    glTranslatef(0, 0, height());
    gluDisk(base, 0.0, radius, 20, 1);
    gluDeleteQuadric(cone);
    gluDeleteQuadric(base);
}