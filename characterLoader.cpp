#include <iostream>
#include <fstream>
#include <sstream>
#include "characterLoader.h"

using namespace std;

void loadBodyPart(Shape *&bodyPart, istringstream& inStream) {
    string shape;
    // Primary colors, secondary colors, and x and y rotations.
    float pR, pG, pB, sR, sG, sB, xR, yR;
    inStream >> shape >> pR >> pG >> pB >> sR >> sG >> sB >> xR >> yR;
    if ("SPHERE" == shape) {
        float radius;
        inStream >> radius;
        Sphere* sphere = new Sphere(pR, pG, pB, sR, sG, sB, xR, yR);
        sphere->setRadius(radius);
        bodyPart = sphere;
    } else if ("CUBE" == shape) {
        float size;
        inStream >> size;
        Cube* cube = new Cube(pR, pG, pB, sR, sG, sB, xR, yR);
        cube->setSize(size);
        bodyPart = cube;
    } else if ("CONE" == shape) {
        float radius, height;
        inStream >> radius >> height;
        Cone* cone = new Cone(pR, pG, pB, sR, sG, sB, xR, yR);
        cone->setRadius(radius);
        cone->setHeight(height);
        bodyPart = cone;
    } else if ("CYLINDER" == shape) {
        float radius, height;
        inStream >> radius >> height;
        Cylinder* cylinder = new Cylinder(pR, pG, pB, sR, sG, sB, xR, yR);
        cylinder->setRadius(radius);
        cylinder->setHeight(height);
        bodyPart = cylinder;
    } else {
        cerr << "Error! Expected a shape but received: " << shape << endl;
    }
}

void loadCharacterFromFile(const char* characterFile, Shape *&torso,
	Shape *&head, Shape *&arm, Shape *&leg, float& distanceBetweenParts) {
    ifstream charFile(characterFile);
    if (charFile.is_open()) {
        cout << "Successfully opened character file " << characterFile << endl;
        string line;
        string firstToken;
        while(getline(charFile, line)) {
            istringstream inStream(line);
            inStream >> firstToken;
            if (line == "" || firstToken == "#") {
                    continue;
            }
            if ("DISTANCE_BETWEEN_PARTS" == firstToken) {
                inStream >> distanceBetweenParts;
                cout << "distanceBetweenParts: " << distanceBetweenParts << endl;
            } else if ("TORSO" == firstToken) {
                loadBodyPart(torso, inStream);
            } else if ("HEAD" == firstToken) {
                loadBodyPart(head, inStream);
            } else if ("ARM" == firstToken) {
                loadBodyPart(arm, inStream);
            } else if ("LEG" == firstToken) {
                loadBodyPart(leg, inStream);
            } else {
                cerr << "Error! Expected a body part but received: " << firstToken << endl;
            }
        }
    } else {
        cerr << "Failed to open character file" << characterFile << endl;
        exit(1);
    }
}