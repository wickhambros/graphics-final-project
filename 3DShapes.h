#ifndef SHAPES_H
#define SHAPES_H

#include "mathfu/vector.h"

typedef mathfu::Vector<float, 3> vec3;

#define DEFAULT_SIZE 0.5

class Shape {
protected:
	vec3 primaryColor;
	vec3 secondaryColor;
	float xRotation; // rotation around x-axis
	float yRotation; // rotation around y-axis

public:
	Shape(): primaryColor(vec3(1.0, 1.0, 1.0)),
		secondaryColor(vec3(0.0, 0.0, 0.0)), xRotation(0.0), yRotation(0.0) {}
	Shape(float pR, float pG, float pB, float sR, float sG, float sB,
		float xR, float yR) {
			setPrimaryColor(vec3(pR, pG, pB));
			setSecondaryColor(vec3(sR, sG, sB));
			setXRotation(xR);
			setYRotation(yR);
	}
	void setPrimaryColor(vec3 color) {
		primaryColor = color;
	}
	void setSecondaryColor(vec3 color) {
		secondaryColor = color;
	}
	void setXRotation(float degreesAroundXAxis) {
		xRotation = degreesAroundXAxis;
	}
	void setYRotation(float degreesAroundYAxis) {
		yRotation = degreesAroundYAxis;
	}
	virtual void draw(void) = 0;
	virtual float width(void) = 0;
	virtual float height(void) = 0;
};

class Cube: public Shape {
private:
	float size;
public:
	Cube(): size(DEFAULT_SIZE) {}
	Cube(float pR, float pG, float pB, float sR, float sG, float sB,
		float xR, float yR): size(DEFAULT_SIZE),
		Shape(pR, pG, pB, sR, sG, sB, xR, yR) {}
	void setSize(float size) {this->size = size;}
	float width(void) {return size;}
	float height(void) {return size;}
	void draw(void);
};

class Sphere: public Shape {
private:
	float radius;
public:
	Sphere(): radius(DEFAULT_SIZE / 2) {}
	Sphere(float pR, float pG, float pB, float sR, float sG, float sB,
		float xR, float yR): radius(DEFAULT_SIZE / 2),
		Shape(pR, pG, pB, sR, sG, sB, xR, yR) {}
	void setRadius(float radius) {this->radius = radius;}
	float width(void) {return 2 * radius;}
	float height(void) {return 2 * radius;}
	void draw(void);
};

class Cone: public Shape {
private:
	float radius, h /*height */;
public:
	Cone(): radius(DEFAULT_SIZE / 2), h(DEFAULT_SIZE) {}
	Cone(float pR, float pG, float pB, float sR, float sG, float sB,
		float xR, float yR): radius(DEFAULT_SIZE / 2), h(DEFAULT_SIZE),
		Shape(pR, pG, pB, sR, sG, sB, xR, yR) {}
	void setRadius(float radius) {this->radius = radius;}
	void setHeight(float height) {h = height;}
	float width(void) {return 2 * radius;}
	float height(void) {return h;}
	void draw(void);
};

class Cylinder: public Shape {
private:
	float radius, h /*height */;
public:
	Cylinder(): radius(DEFAULT_SIZE / 2), h(DEFAULT_SIZE) {}
	Cylinder(float pR, float pG, float pB, float sR, float sG, float sB,
		float xR, float yR): radius(DEFAULT_SIZE / 2), h(DEFAULT_SIZE),
		Shape(pR, pG, pB, sR, sG, sB, xR, yR) {}
	void setRadius(float radius) {this->radius = radius;}
	void setHeight(float height) {h = height;}
	float width(void) {return 2 * radius;}
	float height(void) {return h;}
	void draw(void);
};

#endif