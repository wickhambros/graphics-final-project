LDLIBS = -lglut -lGL -lGLU -lc -lm -lGLEW

INCLUDE = -Imathfu/include -Imathfu/dependencies/vectorial/include

SRC = 3DShapes.cpp characterLoader.cpp drawCharacter.cpp

default:
	make character

.cpp:
	g++ $(SRC) $@.cpp $(LDLIBS) $(INCLUDE) -o $@.out

