# This file specifies the shapes that makes up the character's body parts.
# Each line must be formatted as follows: <body-part> <shape> <primary-color> <secondary-color> <x-Rotation> <y-Rotation> <other-params>
# <body-part> may be one of TORSO, HEAD, ARM, or LEG.
# <shape> may be one of SPHERE, CUBE, CONE, or CYLINDER.
# <primary-color> and <secondary-color> must be 3 floating point values separated by spaces, representing RGB values (e.g. 1.0 0.0 0.0 for red).
# <x-Rotation> and <y-Rotation> must be floating point values, representing the degrees of rotation about the x- and y-axes, respectively.
# <other-params> depends on what shape is being used:
#	SPHERE: <radius>
#	CUBE: <size>
#	CONE: <radius> <height>
#	CYLINDER: <radius> <height>
# Finally, the line "DISTANCE_BETWEEN_PARTS <distance>" specifies how much space should be put between parts when the character is drawn.
# Empty lines and lines starting with '#' will be ignored.


DISTANCE_BETWEEN_PARTS 0.06
TORSO SPHERE 1.0 1.0 0.0 1.0 0.0 0.5 0.0 0.0 0.25
HEAD CUBE 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.25
ARM CONE 0.6 0.49803922 0.0 1.0 0.0 0.0 90.0 0.0 0.09 0.39
LEG CONE 0.0 0.5 0.5 0.4 0.4 1.0 90.0 0.0 0.1 0.2
