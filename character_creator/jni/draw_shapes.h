#ifndef DRAW_SHAPES_H_
#define DRAW_SHAPES_H_

void draw_cube(float size, float pR, float pG, float pB,
        float sR, float sG, float sB);
void draw_cone(float radius, float height, float pR, float pG, float pB,
        float sR, float sG, float sB);
void draw_cylinder(float radius, float height, float pR, float pG, float pB,
        float sR, float sG, float sB);
void draw_sphere(float radius, float pR, float pG, float pB,
        float sR, float sG, float sB);

#endif /* DRAW_SHAPES_H_ */
