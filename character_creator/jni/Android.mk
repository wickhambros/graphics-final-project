LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := opengl
NDK_MODULE_PATH := $(LOCAL_PATH)/..
LOCAL_CPPFLAGS  :=	-I/usr/include \
					-I$(LOCAL_PATH)/mathfu/include \
					-I$(LOCAL_PATH)/mathfu/dependencies/vectorial/include
LOCAL_STATIC_LIBRARIES := freeglut-gles
LOCAL_SRC_FILES := 3DShapes.cpp com_wickhambros_charactercreator_OpenGlJniWrapper.cpp
LOCAL_LDLIBS := -llog -landroid -lGLESv2 -lGLESv1_CM -lEGL -lm

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path,$(LOCAL_PATH)/..)

$(call import-module,freeglut-gles)