#include <GLES/gl.h>
#include "3DShapes.h"
#include "draw_shapes.cpp"

void Cube::draw(void) {
	glPushMatrix();
		glRotatef(xRotation, 1.0, 0.0, 0.0);
		glRotatef(yRotation, 0.0, 1.0, 0.0);
		draw_cube(size, primaryColor.x(), primaryColor.y(), primaryColor.z(),
				secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
	glPopMatrix();
}

void Sphere::draw(void) {
	glPushMatrix();
		glRotatef(xRotation, 1.0, 0.0, 0.0);
		glRotatef(yRotation, 0.0, 1.0, 0.0);
		draw_sphere(radius, primaryColor.x(), primaryColor.y(), primaryColor.z(),
				secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
	glPopMatrix();
}

void Cone::draw(void) {
	glPushMatrix();
		glRotatef(xRotation, 1.0, 0.0, 0.0);
		glRotatef(yRotation, 0.0, 1.0, 0.0);
		draw_cone(radius, h, primaryColor.x(), primaryColor.y(), primaryColor.z(),
				secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
	glPopMatrix();
}

void Cylinder::draw(void) {
	glPushMatrix();
		glRotatef(xRotation, 1.0, 0.0, 0.0);
		glRotatef(yRotation, 0.0, 1.0, 0.0);
		draw_cylinder(radius, h, primaryColor.x(), primaryColor.y(), primaryColor.z(),
				secondaryColor.x(), secondaryColor.y(), secondaryColor.z());
	glPopMatrix();
}
