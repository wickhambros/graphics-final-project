#include "draw_shapes.h"
#include <GLES/gl.h>
#include <iostream>
#include <vector>
#include <math.h>

using std::cout;
using std::endl;

#define CUBE_VERTEX(i) cubeVertices[i][0], cubeVertices[i][1], cubeVertices[i][2]

#define PI 3.14159265
// Cylinder constants.
#define NUM_CIRCLES 2
#define NUM_LINES 10
#define NUM_POINTS_ON_CIRCLE 28
// Sphere constants.
#define SPHERE_NUM_POINTS 15

#define PI 3.14159265
#define SMALL_SIZE_OFFSET 0.001
// Cylinder constants.
#define NUM_POINTS_ON_CIRCLE 28
#define CYLINDER_NUM_CIRCLES 2  // must be at least 2 (top and bottom).
#define CYLINDER_NUM_LINES 6  // should be even.
// Sphere constants.
#define SPHERE_NUM_POINTS 15
// Cube stuff.
#define CUBE_VERTEX(i) cubeVertices[i][0], cubeVertices[i][1], cubeVertices[i][2]

using std::cout;
using std::endl;

// Expects 4 3-space vertices forming a Z shape.
void drawSolidRectangle(float* vertices) {
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);
}

// Expects a 3-space point in the middle of the circle and NUM_POINTS_ON_CIRCLE
// vertices (in order, clockwise or counterclockwise). First vertex == last.
void drawSolidCircle(float* vertices) {
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_TRIANGLE_FAN, 0, NUM_POINTS_ON_CIRCLE + 2);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void drawWireCylinder(float bottomRadius, float topRadius, float height) {
    GLfloat circlePoints[NUM_POINTS_ON_CIRCLE*3];
    float dRadius = (topRadius-bottomRadius) / (CYLINDER_NUM_CIRCLES-1);
    float dTheta = 2.0*PI/NUM_POINTS_ON_CIRCLE;
    float dZ = height/(CYLINDER_NUM_CIRCLES-1);
    float radius = bottomRadius;
    float z = -height/2;
    // Draw the circles along the z axis, centered at 0.
    for (int c = 0; c < CYLINDER_NUM_CIRCLES; c++) {
        float theta = 0;
        for (int i = 0; i < NUM_POINTS_ON_CIRCLE; i++) {
            circlePoints[i*3 + 0] = radius*cos(theta);
            circlePoints[i*3 + 1] = radius*sin(theta);
            circlePoints[i*3 + 2] = z;
            theta += dTheta;
        }
        glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, circlePoints);
            glDrawArrays(GL_LINE_LOOP, 0, NUM_POINTS_ON_CIRCLE);
        glDisableClientState(GL_VERTEX_ARRAY);
        radius += dRadius;
        z += dZ;
    }
    // Draw lines along the edge of the cylinder.
    GLfloat lineEndpoints[CYLINDER_NUM_LINES*6];  // 2 endpoints in 3-space each.
    float theta = 0;
    dTheta = 2.0*PI/CYLINDER_NUM_LINES;
    float minZ = -height/2;
    float maxZ = height/2;
    for (int i = 0; i < CYLINDER_NUM_LINES; i++) {
        lineEndpoints[i*6 + 0] = bottomRadius*cos(theta);
        lineEndpoints[i*6 + 1] = bottomRadius*sin(theta);
        lineEndpoints[i*6 + 2] = minZ;
        lineEndpoints[i*6 + 3] = topRadius*cos(theta);
        lineEndpoints[i*6 + 4] = topRadius*sin(theta);
        lineEndpoints[i*6 + 5] = maxZ;
        theta += dTheta;
    }
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, lineEndpoints);
        glDrawArrays(GL_LINES, 0, CYLINDER_NUM_LINES*2);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void drawSolidCylinder(float bottomRadius, float topRadius, float height,
        float sR, float sG, float sB) {
    bottomRadius -= SMALL_SIZE_OFFSET;
    topRadius -= SMALL_SIZE_OFFSET;
    height -= SMALL_SIZE_OFFSET;
    // Draw the rectangles along the side of the cylinder.
    GLfloat rectanglePoints[12];
    float dTheta = 2.0*PI/NUM_POINTS_ON_CIRCLE;
    for (float theta = 0; theta < 2*PI; ) {
        rectanglePoints[0] = bottomRadius*cos(theta);
        rectanglePoints[1] = bottomRadius*sin(theta);
        rectanglePoints[2] = -height/2;
        rectanglePoints[6] = topRadius*cos(theta);
        rectanglePoints[7] = topRadius*sin(theta);
        rectanglePoints[8] = height/2;
        theta += dTheta;
        rectanglePoints[3] = bottomRadius*cos(theta);
        rectanglePoints[4] = bottomRadius*sin(theta);
        rectanglePoints[5] = -height/2;
        rectanglePoints[9] = topRadius*cos(theta);
        rectanglePoints[10] = topRadius*sin(theta);
        rectanglePoints[11] = height/2;
        drawSolidRectangle(rectanglePoints);
    }
    // Draw the circle on bottom, in the secondary color.
    glColor4f(sR, sG, sB, 1.0);
    GLfloat circlePoints[(NUM_POINTS_ON_CIRCLE + 2) * 3];
    // We need a point inside the circle to start. Use the middle one.
    circlePoints[0] = circlePoints[1] = 0;
    circlePoints[2] = -height/2;
    float theta = 0;
    for (int i = 1; i < NUM_POINTS_ON_CIRCLE + 2; i++) {
        circlePoints[i*3 + 0] = bottomRadius*cos(theta);
        circlePoints[i*3 + 1] = bottomRadius*sin(theta);
        circlePoints[i*3 + 2] = -height/2;
        theta += dTheta;
    }
    drawSolidCircle(circlePoints);
    // Draw the circle on top.
    for (int i = 1; i < NUM_POINTS_ON_CIRCLE + 2; i++) {
        circlePoints[i*3 + 0] = topRadius*cos(theta);
        circlePoints[i*3 + 1] = topRadius*sin(theta);
        circlePoints[i*3 + 2] = height/2;
        theta += dTheta;
    }
    drawSolidCircle(circlePoints);
}

void drawWireSphere(float radius) {
    GLfloat circlePointsZ[SPHERE_NUM_POINTS*3];
    GLfloat circlePointsY[SPHERE_NUM_POINTS*3];
    float dPhi = PI/(SPHERE_NUM_POINTS-1);
    float dTheta = 2.0*PI/SPHERE_NUM_POINTS;
    float thetaZ = 0;
    float phiY = 0;
    // Draw the lines rotating around z using phi and theta.
    for (int c = 0; c < SPHERE_NUM_POINTS; c++) {
        float phiZ = 0;
        float thetaY = 0;
        for (int i = 0; i < SPHERE_NUM_POINTS; i++) {
            circlePointsZ[i*3 + 0] = radius*cos(thetaZ)*sin(phiZ);
            circlePointsZ[i*3 + 1] = radius*sin(thetaZ)*sin(phiZ);
            circlePointsZ[i*3 + 2] = radius*cos(phiZ);
            circlePointsY[i*3 + 0] = radius*cos(thetaY)*sin(phiY);
            circlePointsY[i*3 + 1] = radius*sin(thetaY)*sin(phiY);
            circlePointsY[i*3 + 2] = radius*cos(phiY);
            phiZ += dPhi;
            thetaY += dTheta;
        }
        glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, circlePointsZ);
            glDrawArrays(GL_LINE_STRIP, 0, SPHERE_NUM_POINTS);
            glVertexPointer(3, GL_FLOAT, 0, circlePointsY);
            glDrawArrays(GL_LINE_LOOP, 0, SPHERE_NUM_POINTS);
        glDisableClientState(GL_VERTEX_ARRAY);
        thetaZ += dTheta;
        phiY += dPhi;
    }
}

void drawSolidSphere(float radius) {
    radius -= SMALL_SIZE_OFFSET;
    GLfloat rectanglePoints[12];
    float dPhi = PI/(SPHERE_NUM_POINTS-1);
    float dTheta = 2.0*PI/SPHERE_NUM_POINTS;
    // Draw the lines rotating around z using phi and theta.
    for (float theta = 0; theta < 2.0*PI; theta += dTheta) {
        for (float phi = 0; phi < PI - dPhi; phi += dPhi) {
            rectanglePoints[0] = radius*cos(theta)*sin(phi);
            rectanglePoints[1] = radius*sin(theta)*sin(phi);
            rectanglePoints[2] = radius*cos(phi);
            rectanglePoints[3] = radius*cos(theta + dTheta)*sin(phi);
            rectanglePoints[4] = radius*sin(theta + dTheta)*sin(phi);
            rectanglePoints[5] = radius*cos(phi);
            rectanglePoints[6] = radius*cos(theta)*sin(phi + dPhi);
            rectanglePoints[7] = radius*sin(theta)*sin(phi + dPhi);
            rectanglePoints[8] = radius*cos(phi + dPhi);
            rectanglePoints[9] = radius*cos(theta + dTheta)*sin(phi + dPhi);
            rectanglePoints[10] = radius*sin(theta + dTheta)*sin(phi + dPhi);
            rectanglePoints[11] = radius*cos(phi + dPhi);
            drawSolidRectangle(rectanglePoints);
        }
    }
}

void drawWireCube(float sideLength) {
    sideLength = sideLength / 2;
    GLfloat cubeVertices[8][3] = {
        {-sideLength, -sideLength, -sideLength},    //0: bottom back left
        {-sideLength, -sideLength, sideLength},     //1: bottom front left
        {-sideLength, sideLength, -sideLength},     //2: top back left
        {-sideLength, sideLength, sideLength},      //3: top front left
        {sideLength, -sideLength, -sideLength},     //4: bottom back right
        {sideLength, -sideLength, sideLength},      //5: bottom front right
        {sideLength, sideLength, -sideLength},      //6: top back right
        {sideLength, sideLength, sideLength},       //7: top front right
    };

    glEnableClientState(GL_VERTEX_ARRAY);

    GLfloat cubeFront[] = {
        CUBE_VERTEX(7),
        CUBE_VERTEX(3),
        CUBE_VERTEX(1),
        CUBE_VERTEX(5)
    };
    glVertexPointer(3, GL_FLOAT, 0, cubeFront);
    glDrawArrays(GL_LINE_LOOP, 0, 4);

    GLfloat cubeRight[] = {
        CUBE_VERTEX(7),
        CUBE_VERTEX(5),
        CUBE_VERTEX(4),
        CUBE_VERTEX(6)
    };
    glVertexPointer(3, GL_FLOAT, 0, cubeRight);
    glDrawArrays(GL_LINE_LOOP, 0, 4);

    GLfloat cubeLeft[] = {
        CUBE_VERTEX(3),
        CUBE_VERTEX(1),
        CUBE_VERTEX(0),
        CUBE_VERTEX(2)
    };
    glVertexPointer(3, GL_FLOAT, 0, cubeLeft);
    glDrawArrays(GL_LINE_LOOP, 0, 4);

    GLfloat cubeBack[] = {
        CUBE_VERTEX(0),
        CUBE_VERTEX(2),
        CUBE_VERTEX(6),
        CUBE_VERTEX(4)
    };
    glVertexPointer(3, GL_FLOAT, 0, cubeBack);
    glDrawArrays(GL_LINE_LOOP, 0, 4);

    glDisableClientState(GL_VERTEX_ARRAY);
}

void drawSolidCube(float sideLength) {
    sideLength -= SMALL_SIZE_OFFSET;
    sideLength /= 2;
    GLfloat cubeVertices[8][3] = {
        {-sideLength, -sideLength, -sideLength},    //0: bottom back left
        {-sideLength, -sideLength, sideLength},     //1: bottom front left
        {-sideLength, sideLength, -sideLength},     //2: top back left
        {-sideLength, sideLength, sideLength},      //3: top front left
        {sideLength, -sideLength, -sideLength},     //4: bottom back right
        {sideLength, -sideLength, sideLength},      //5: bottom front right
        {sideLength, sideLength, -sideLength},      //6: top back right
        {sideLength, sideLength, sideLength},       //7: top front right
    };

    // Draw front.
    GLfloat frontPoints[12] = {
        CUBE_VERTEX(3),
        CUBE_VERTEX(7),
        CUBE_VERTEX(1),
        CUBE_VERTEX(5)
    };
    drawSolidRectangle(frontPoints);

    // Draw left.
    GLfloat leftPoints[12] = {
        CUBE_VERTEX(2),
        CUBE_VERTEX(3),
        CUBE_VERTEX(0),
        CUBE_VERTEX(1)
    };
    drawSolidRectangle(leftPoints);

    // Draw right.
    GLfloat rightPoints[12] = {
        CUBE_VERTEX(7),
        CUBE_VERTEX(6),
        CUBE_VERTEX(5),
        CUBE_VERTEX(4)
    };
    drawSolidRectangle(rightPoints);

    // Draw top.
    GLfloat topPoints[12] = {
        CUBE_VERTEX(2),
        CUBE_VERTEX(6),
        CUBE_VERTEX(3),
        CUBE_VERTEX(7)
    };
    drawSolidRectangle(topPoints);

    // Draw bottom.
    GLfloat bottomPoints[12] = {
        CUBE_VERTEX(1),
        CUBE_VERTEX(5),
        CUBE_VERTEX(0),
        CUBE_VERTEX(4)
    };
    drawSolidRectangle(bottomPoints);

    // Draw back.
    GLfloat backPoints[12] = {
        CUBE_VERTEX(2),
        CUBE_VERTEX(6),
        CUBE_VERTEX(0),
        CUBE_VERTEX(4)
    };
    drawSolidRectangle(backPoints);
}

void drawCylinder(float bottomRadius, float topRadius, float height,
        float pR, float pG, float pB, float sR, float sG, float sB) {
    glRotatef(90, 1, 0, 0);
    glColor4f(pR, pG, pB, 1.0);
    drawSolidCylinder(bottomRadius, topRadius, height, sR, sG, sB);
    glColor4f(sR, sG, sB, 1.0);
    drawWireCylinder(bottomRadius, topRadius, height);
}

void draw_cone(float radius, float height,
		float pR, float pG, float pB, float sR, float sG, float sB) {
	drawCylinder(radius, 0, height, pR, pG, pB, sR, sG, sB);
}

void draw_cylinder(float radius, float height,
		float pR, float pG, float pB, float sR, float sG, float sB) {
	drawCylinder(radius, radius, height, pR, pG, pB, sR, sG, sB);
}

void draw_sphere(float radius, float pR, float pG, float pB,
        float sR, float sG, float sB) {
    glColor4f(pR, pG, pB, 1.0);
    drawSolidSphere(radius);
    glColor4f(sR, sG, sB, 1.0);
    drawWireSphere(radius);
}

void draw_cube(float sideLength, float pR, float pG, float pB,
        float sR, float sG, float sB) {
    glColor4f(pR, pG, pB, 1.0);
    drawSolidCube(sideLength);
    glColor4f(sR, sG, sB, 1.0);
    drawWireCube(sideLength);
}
