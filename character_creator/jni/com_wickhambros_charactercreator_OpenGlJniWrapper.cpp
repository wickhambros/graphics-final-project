#include "com_wickhambros_charactercreator_OpenGlJniWrapper.h"
#include <android/log.h>
#include <GLES/gl.h>
//#include <GL/freeglut.h>
//#include <GL/freeglut_ext.h>
#include "3DShapes.h"

using namespace std;

Shape *torso, *head, *arm, *leg;
enum BodyPartEnum {TORSO=0, HEAD=1, ARM=2, LEG=3};
enum ShapeEnum {CUBE=0, SPHERE=1, CONE=2, CYLINDER=3};
double distanceBetweenParts = 0.05;

double size=0.25, r, g, b;
float rotationMatrix[16];

void drawCharacter(void) {
	glPushMatrix();
		torso->draw();
		glPushMatrix();
			float distToTopOfTorso = torso->height() / 2 + head->height() / 2;
			glTranslatef(0, distToTopOfTorso + distanceBetweenParts, 0);
			head->draw();
		glPopMatrix();
		float distToSideOfTorso = torso->width() / 2 + arm->width() / 2;
		glPushMatrix();
			glTranslatef(distToSideOfTorso + distanceBetweenParts, 0, 0);
			arm->draw();
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-distToSideOfTorso - distanceBetweenParts, 0, 0);
			arm->draw();
		glPopMatrix();
		float distToBottomOfTorso = torso->height() / 2 + leg->height() / 2;
		glPushMatrix();
			glTranslatef(-(distanceBetweenParts + leg->height()) / 2,
				-distToBottomOfTorso - distanceBetweenParts,
				0);
			leg->draw();
		glPopMatrix();
		glPushMatrix();
			glTranslatef((distanceBetweenParts + leg->height()) / 2,
				-distToBottomOfTorso - distanceBetweenParts, 0);
			leg->draw();
		glPopMatrix();
	glPopMatrix();
}

void setBodyPart(Shape *&bodyPart, ShapeEnum shape, float pR, float pG, float pB,
		float sR, float sG, float sB, float xR, float yR, float otherParams[]) {
	if (SPHERE == shape) {
		float radius = otherParams[0];
		Sphere* sphere = new Sphere(pR, pG, pB, sR, sG, sB, xR, yR);
		sphere->setRadius(radius);
		bodyPart = sphere;
	} else if (CUBE == shape) {
		float size = otherParams[0];
		Cube* cube = new Cube(pR, pG, pB, sR, sG, sB, xR, yR);
		cube->setSize(size);
		bodyPart = cube;
	} else if (CONE == shape) {
		float radius = otherParams[0];
		float height = otherParams[1];
		Cone* cone = new Cone(pR, pG, pB, sR, sG, sB, xR, yR);
		cone->setRadius(radius);
		cone->setHeight(height);
		bodyPart = cone;
	} else if (CYLINDER == shape) {
		float radius = otherParams[0];
		float height = otherParams[1];
		Cylinder* cylinder = new Cylinder(pR, pG, pB, sR, sG, sB, xR, yR);
		cylinder->setRadius(radius);
		cylinder->setHeight(height);
		bodyPart = cylinder;
	}
}
// on_surface_created()
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_on_1surface_1created
  (JNIEnv *, jclass) {
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_CULL_FACE);
}

// on_surface_changed(int width, int height)
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_on_1surface_1changed
  (JNIEnv *, jclass, jint width, jint height) {
	// No-op.
}

// on_draw_frame()
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_on_1draw_1frame
  (JNIEnv *, jclass) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
		glRotatef(30, 0, 1, 0);
		drawCharacter();
	glPopMatrix();
}

// multiply_rotation(float angleX, float angleY)
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_multiply_1rotation
  (JNIEnv *, jclass, jfloat angleX, jfloat angleY) {
	// Store accumulated rotation, then apply the new rotation for this frame multiplied by it.
	glGetFloatv(GL_MODELVIEW_MATRIX, rotationMatrix);
	glLoadIdentity();
	glRotatef(angleX, 1, 0, 0);
	glRotatef(angleY, 0, 1, 0);
	glMultMatrixf(rotationMatrix);
}

// update_body_part(int bodyPart, float pR, float pG, float pB,
// float sR, sG, sB, float xR, float yR, float[] otherParams)
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_update_1body_1part
  (JNIEnv * env, jclass, jint bodyPartIndex, jint shapeIndex, jfloat pR, jfloat pG, jfloat pB,
		  jfloat sR, jfloat sG, jfloat sB, jfloat xR, jfloat yR, jfloatArray otherParams) {
	// First, extract otherParams array.
	jfloat* jOtherParameters = env->GetFloatArrayElements(otherParams, NULL);
	float* otherParameters = jOtherParameters;
	env->ReleaseFloatArrayElements(otherParams, jOtherParameters, JNI_ABORT);
	switch (bodyPartIndex) {
	case TORSO: setBodyPart(torso, ShapeEnum(shapeIndex),
			pR, pG, pB, sR, sG, sB, xR, yR, otherParameters); break;
	case HEAD: setBodyPart(head, ShapeEnum(shapeIndex),
			pR, pG, pB, sR, sG, sB, xR, yR, otherParameters); break;
	case ARM: setBodyPart(arm, ShapeEnum(shapeIndex),
			pR, pG, pB, sR, sG, sB, xR, yR, otherParameters); break;
	case LEG: setBodyPart(leg, ShapeEnum(shapeIndex),
			pR, pG, pB, sR, sG, sB, xR, yR, otherParameters); break;
	}
}

// set_size()
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_set_1size
  (JNIEnv *, jclass, jdouble newSize) {
	size = newSize;
}

// set_dist_between_parts()
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_set_1dist_1between_1parts
  (JNIEnv *, jclass, jdouble newDistBetweenParts) {
	distanceBetweenParts = newDistBetweenParts;
}

// set_color(double r, double g, double b)
JNIEXPORT void JNICALL Java_com_wickhambros_charactercreator_OpenGlJniWrapper_set_1color
  (JNIEnv *, jclass, jdouble newR, jdouble newG, jdouble newB) {
	r = newR;
	g = newG;
	b = newB;
}
