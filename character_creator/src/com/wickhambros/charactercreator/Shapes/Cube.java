package com.wickhambros.charactercreator.Shapes;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Cube extends Shape {
	private static final float MIN_SIZE = 0.10f;
	
	private SeekBar sizeSeekBar;
	
	public Cube() {
		super();
		otherParams = new float[] {0.25f}; // size
	}
	
	public Cube(float pR, float pG, float pB, float sR, float sG, float sB, float xR, float yR,
			float size) {
		super(pR, pG, pB, sR, sG, sB, xR, yR);
		this.otherParams = new float[] {size};
	}
	
	public Cube(final SeekBar sizeSeekBar) {
		this();
		this.sizeSeekBar = sizeSeekBar;
	}
	
	public int getShapeId() {
		return Shape.CUBE;
	}
	
	public float[] getValues() {
		float[] values = new float[NUM_SHAPE_VALUES + 1];
		values[0] = pR; values[1] = pG; values[2] = pB;
		values[3] = sR; values[4] = sG; values[5] = sB;
		values[6] = xR; values[7] = yR;
		values[8] = otherParams[0];
		return values;
	}
	
	protected void handleUiElements(int bodyPartId) {
		handleSizeSeekBar(sizeSeekBar, bodyPartId);
	}

	protected void updateUiElements() {
		if (this.sizeSeekBar != null) {
			this.sizeSeekBar.setProgress((int) ((otherParams[0]  - MIN_SIZE) * 100));
		}
	}
	
	private void handleSizeSeekBar(final SeekBar sizeSeekBar, final int bodyPartId) {
		sizeSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				float size = progress / 100.0f + MIN_SIZE;
				otherParams[0] = size;
				updateBodyPart(bodyPartId);
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
	}
}
