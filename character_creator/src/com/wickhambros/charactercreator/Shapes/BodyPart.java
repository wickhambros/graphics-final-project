package com.wickhambros.charactercreator.Shapes;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;

import com.wickhambros.charactercreator.R;
import com.wickhambros.charactercreator.Utils.UserInterfaceUtils;

public class BodyPart {
	public static final int TORSO = 0;
	public static final int HEAD = 1;
	public static final int ARM = 2;
	public static final int LEG = 3;

	public Shape shape;
	private int bodyPartId;
	
	public BodyPart(int bodyPartId, Shape shape) {
		this.bodyPartId = bodyPartId;
		this.shape = shape;
	}
	
	public void setShape(Shape shape) {
		if (this.shape != null) {
			// Keep body part's color and rotation.
			shape.setPrimaryColor(this.shape.pR, this.shape.pG, this.shape.pB);
			shape.setSecondaryColor(this.shape.sR, this.shape.sG, this.shape.sB);
			shape.setRotation(this.shape.xR, this.shape.yR);
			if (this.shape.getShapeId() == shape.getShapeId()) {
				shape.otherParams = this.shape.otherParams;
			}
		}
		this.shape = shape;
		this.shape.updateUiElements();
		this.shape.handleUiElements(bodyPartId);
	}
	
	/**
	 * Update the drawn body part on the GlSurfaceView.
	 */
	public void updateBodyPartOnScreen() {
		shape.updateBodyPart(bodyPartId);
	}
	
	/**
	 * Sets UI elements (i.e. SeekBars and EditTexts) to corresponding values.
	 */
	public void setUiElements(ViewGroup mainLayout, boolean isPrimaryColor) {
		Spinner shapesSpinner = (Spinner) mainLayout.findViewById(R.id.shapesSpinner);
		shapesSpinner.setSelection(shape.getShapeId(), true);
		
		// Make sure interface updates this body part's shape.
		UserInterfaceUtils.onShapeSelected(shape.getShapeId(), mainLayout);

		// SeekBars will be updated when EditTexts are.
		EditText redEdit = (EditText) mainLayout.findViewById(R.id.redEdit);
		redEdit.setText(String.valueOf((int) ((isPrimaryColor ? shape.pR : shape.sR) * 255)));
		EditText greenEdit = (EditText) mainLayout.findViewById(R.id.greenEdit);
		greenEdit.setText(String.valueOf((int) ((isPrimaryColor ? shape.pG : shape.sG) * 255)));
		EditText blueEdit = (EditText) mainLayout.findViewById(R.id.blueEdit);
		blueEdit.setText(String.valueOf((int) ((isPrimaryColor ? shape.pB : shape.sB) * 255)));
		EditText xRotEdit = (EditText) mainLayout.findViewById(R.id.xRotEdit);
		xRotEdit.setText(String.valueOf((int) (shape.xR)));
		EditText yRotEdit = (EditText) mainLayout.findViewById(R.id.yRotEdit);
		yRotEdit.setText(String.valueOf((int) (shape.yR)));
	}
	
	public void handleRedSeekBarAndEditText(final SeekBar red, final EditText redEdit) {
		red.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				if (UserInterfaceUtils.isPrimaryColor) shape.pR = progress / 255.0f;
				else shape.sR = progress / 255.0f;
				updateBodyPartOnScreen();
		        if (fromUser) redEdit.setText(String.valueOf(progress));
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
		
		redEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				red.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	public void handleGreenSeekBarAndEditText(final SeekBar green, final EditText greenEdit) {
		green.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				if (UserInterfaceUtils.isPrimaryColor) shape.pG = progress / 255.0f;
				else shape.sG = progress / 255.0f;
				updateBodyPartOnScreen();
		        if (fromUser) greenEdit.setText(String.valueOf(progress));
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
		
		greenEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				green.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	public void handleBlueSeekBarAndEditText(final SeekBar blue, final EditText blueEdit) {
		blue.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				if (UserInterfaceUtils.isPrimaryColor) shape.pB = progress / 255.0f;
				else shape.sB = progress / 255.0f;
				updateBodyPartOnScreen();
		        if (fromUser) blueEdit.setText(String.valueOf(progress));
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
		
		blueEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				blue.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	public void handleXRotSeekBarAndEditText(final SeekBar xRot, final EditText xRotEdit) {
		xRot.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				shape.xR = progress;
				updateBodyPartOnScreen();
		        if (fromUser) xRotEdit.setText(String.valueOf(progress));
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
		
		xRotEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				xRot.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
	
	public void handleYRotSeekBarAndEditText(final SeekBar yRot, final EditText yRotEdit) {
		yRot.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				shape.yR = progress;
				updateBodyPartOnScreen();
		        if (fromUser) yRotEdit.setText(String.valueOf(progress));
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
		
		yRotEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				yRot.setProgress(s.length() == 0 ? 0 : Integer.parseInt(s.toString()));
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}
}
