package com.wickhambros.charactercreator.Shapes;

import com.wickhambros.charactercreator.OpenGlJniWrapper;


abstract public class Shape {
	public static final int CUBE = 0;
	public static final int SPHERE = 1;
	public static final int CONE = 2;
	public static final int CYLINDER = 3;

	protected float pR, pG, pB, sR, sG, sB, xR, yR;
	protected float[] otherParams;
	
	protected static final int NUM_SHAPE_VALUES = 8;
	
	public Shape() {
		pR = pG = pB = sR = sG = sB = xR = yR = 0;
	}
	
	public Shape(float pR, float pG, float pB, float sR, float sG, float sB, float xR, float yR) {
		setPrimaryColor(pR, pG, pB);
		setSecondaryColor(sR, sG, sB);
		setRotation(xR, yR);
	}
	
	public void setPrimaryColor(float r, float g, float b) {
		this.pR = r;
		this.pG = g;
		this.pB = b;
	}
	
	public void setSecondaryColor(float r, float g, float b) {
		this.sR = r;
		this.sG = g;
		this.sB = b;
	}
	
	public void setRotation(float xR, float yR) {
		this.xR = xR;
		this.yR = yR;
	}
	
	public int getShapeId() {
		return Shape.CUBE;
	}
	
	/**
	 * Update shape attributes such as color and rotation when UI elements are  used.
	 */
	abstract protected void handleUiElements(int bodyPartId);
	
	/**
	 * Update UI elements such as SeekBars to corresponding values.
	 */
	abstract protected void updateUiElements();
	
	/**
	 * Returns an array of all the values for this shape.
	 */
	abstract public float[] getValues();
	
	/**
	 * Updates the body part that this shape represents.
	 */
	public void updateBodyPart(int bodyPart) {
		OpenGlJniWrapper.update_body_part(bodyPart, getShapeId(), pR, pG, pB,
				sR, sG, sB, xR, yR, otherParams);
	}
}
