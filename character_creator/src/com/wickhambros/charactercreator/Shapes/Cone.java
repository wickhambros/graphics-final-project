package com.wickhambros.charactercreator.Shapes;

import com.wickhambros.charactercreator.Utils.CharacterExporterUtils;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Cone extends Shape {
	private static final float MIN_DIAMETER = 0.10f;
	private static final float MIN_HEIGHT = MIN_DIAMETER;
	
	private SeekBar diameterSeekBar, heightSeekBar;
	
	public Cone() {
		super();
		otherParams = new float[] {0.125f, 0.25f}; // diameter, height
	}
	
	public Cone(float pR, float pG, float pB, float sR, float sG, float sB, float xR, float yR,
			float radius, float height) {
		super(pR, pG, pB, sR, sG, sB, xR, yR);
		this.otherParams = new float[] {radius, height};
	}
	
	public Cone(final SeekBar diameter, final SeekBar height) {
		this();
		this.diameterSeekBar = diameter;
		this.heightSeekBar = height;
	}
	
	public int getShapeId() {
		return Shape.CONE;
	}
	
	public float[] getValues() {
		float[] values = new float[NUM_SHAPE_VALUES + 2];
		values[0] = pR; values[1] = pG; values[2] = pB;
		values[3] = sR; values[4] = sG; values[5] = sB;
		values[6] = xR + CharacterExporterUtils.CONE_CYLINDER_X_ROTATION_CORRECTION;
		values[7] = yR;
		values[8] = otherParams[0]; values[9] = otherParams[1];
		return values;
	}
	
	protected void handleUiElements(int bodyPartId) {
		handleDiameterSeekBar(diameterSeekBar, bodyPartId);
		handleHeightSeekBar(heightSeekBar, bodyPartId);
	}

	protected void updateUiElements() {
		if (this.diameterSeekBar != null && this.heightSeekBar != null) {
			this.diameterSeekBar.setProgress((int) (((2 * otherParams[0]) -  MIN_DIAMETER) * 100));
			this.heightSeekBar.setProgress((int) ((otherParams[1]  - MIN_HEIGHT) * 100));
		}
	}
	
	private void handleDiameterSeekBar(final SeekBar diameterSeekBar, final int bodyPartId) {
		diameterSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				float radius = (progress / 100.0f + MIN_DIAMETER) / 2;
				otherParams[0] = radius;
				updateBodyPart(bodyPartId);
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
	}
	
	private void handleHeightSeekBar(final SeekBar heightSeekBar, final int bodyPartId) {
		heightSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				float height = progress / 100.0f + MIN_HEIGHT;
				otherParams[1] = height;
				updateBodyPart(bodyPartId);
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
	}
}
