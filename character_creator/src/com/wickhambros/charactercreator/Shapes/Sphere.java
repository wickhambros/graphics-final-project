package com.wickhambros.charactercreator.Shapes;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Sphere extends Shape {
	private static final float MIN_DIAMETER = 0.10f;
	
	private SeekBar diameterSeekBar;
	
	public Sphere(final SeekBar diameter) {
		super();
		otherParams = new float[] {0.125f}; // radius
		this.diameterSeekBar = diameter;
	}
	
	public Sphere(float pR, float pG, float pB, float sR, float sG, float sB, float xR, float yR,
			float radius) {
		super(pR, pG, pB, sR, sG, sB, xR, yR);
		this.otherParams = new float[] {radius};
	}
	
	public int getShapeId() {
		return Shape.SPHERE;
	}
	
	public float[] getValues() {
		float[] values = new float[NUM_SHAPE_VALUES + 1];
		values[0] = pR; values[1] = pG; values[2] = pB;
		values[3] = sR; values[4] = sG; values[5] = sB;
		values[6] = xR; values[7] = yR;
		values[8] = otherParams[0];
		return values;
	}
	
	protected void handleUiElements(int bodyPartId) {
		handleDiameterSeekBar(diameterSeekBar, bodyPartId);
	}

	protected void updateUiElements() {
		if (this.diameterSeekBar != null) {
			this.diameterSeekBar.setProgress((int) (((2 * otherParams[0]) -  MIN_DIAMETER) * 100));
		}
	}
	
	private void handleDiameterSeekBar(final SeekBar diameterSeekBar, final int bodyPartId) {
		diameterSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				float radius = (progress / 100.0f + MIN_DIAMETER) / 2;
				otherParams[0] = radius;
				updateBodyPart(bodyPartId);
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
	}
}
