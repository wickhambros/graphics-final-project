package com.wickhambros.charactercreator.Shapes;

public class Character {
	// Default BodyParts.
	public static final BodyPart TORSO = new BodyPart(BodyPart.TORSO,
				new Sphere(1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.25f));
	public static BodyPart HEAD = new BodyPart(BodyPart.HEAD,
			new Cube(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.25f));
	public static BodyPart ARM = new BodyPart(BodyPart.ARM,
			new Cylinder(0.0f, 0.5f, 0.0f, 0.0f, 0.4f, 0.0f, 90.0f, 0.0f, 0.1f, 0.2f));
	public static BodyPart LEG = new BodyPart(BodyPart.LEG,
			new Cone(0.0f, 0.5f, 0.5f, 0.4f, 0.4f, 1.0f, 0.0f, 0.0f, 0.1f, 0.2f));
	
	public static void updateAllBodyPartsOnScreen() {
		TORSO.updateBodyPartOnScreen();
		HEAD.updateBodyPartOnScreen();
		ARM.updateBodyPartOnScreen();
		LEG.updateBodyPartOnScreen();
	}
}
