package com.wickhambros.charactercreator;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

import com.wickhambros.charactercreator.Shapes.Character;
import com.wickhambros.charactercreator.Utils.RotationUtils;

public class RendererWrapper implements Renderer {
	@Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	    OpenGlJniWrapper.on_surface_created();
	    Character.updateAllBodyPartsOnScreen();
    }
 
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
    	gl.glViewport(0, 0, width, height);
    	gl.glMatrixMode(GL10.GL_PROJECTION);
    	gl.glLoadIdentity();
    	GLU.gluPerspective(gl, 60.0f, (float) width / (float) height, 0.1f, 3.0f);
    	GLU.gluLookAt(gl, 0,0,2 , 0,0,0 , 0,1,0);
    	gl.glMatrixMode(GL10.GL_MODELVIEW);
    	gl.glLoadIdentity();

        OpenGlJniWrapper.on_surface_changed(width, height);
    }
 
    @Override
    public void onDrawFrame(GL10 gl) {
    	RotationUtils.accumulateRotations();
        OpenGlJniWrapper.on_draw_frame();
    }
}
