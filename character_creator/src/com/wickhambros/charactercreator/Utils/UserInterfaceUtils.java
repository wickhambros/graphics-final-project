package com.wickhambros.charactercreator.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;

import com.wickhambros.charactercreator.OpenGlJniWrapper;
import com.wickhambros.charactercreator.R;
import com.wickhambros.charactercreator.Shapes.BodyPart;
import com.wickhambros.charactercreator.Shapes.Character;
import com.wickhambros.charactercreator.Shapes.Cone;
import com.wickhambros.charactercreator.Shapes.Cube;
import com.wickhambros.charactercreator.Shapes.Cylinder;
import com.wickhambros.charactercreator.Shapes.Shape;
import com.wickhambros.charactercreator.Shapes.Sphere;

public class UserInterfaceUtils {
	public static boolean isPrimaryColor = true;
	
	private static Context context;
	
	// Store the UI elements that are used for all body parts for future use.
	private static SeekBar redSeekBar;
	private static EditText redEditText;
	private static SeekBar greenSeekBar;
	private static EditText greenEditText;
	private static SeekBar blueSeekBar;
	private static EditText blueEditText;
	private static SeekBar xRotSeekBar;
	private static EditText xRotEditText;
	private static SeekBar yRotSeekBar;
	private static EditText yRotEditText;

	private static BodyPart currBodyPart = Character.TORSO;

	public static void handleDistBetweenPartsSeekBar(final SeekBar distBetweenParts) {
        distBetweenParts.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				OpenGlJniWrapper.set_dist_between_parts(progress / 100.0);
			}
			@Override public void onStartTrackingTouch(SeekBar arg0) {}
			@Override public void onStopTrackingTouch(SeekBar arg0) {}
        });
	}
	
	public static void handleBodyPartsSpinner(Context c, final Spinner bodyParts) {
		context = c;
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
		        R.array.body_parts, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bodyParts.setAdapter(adapter);
		bodyParts.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				final ViewGroup mainLayout = (ViewGroup) bodyParts.getParent().getParent();
				// Set radio button to Primary Colors.
				((RadioButton) mainLayout.findViewById(R.id.primaryColor)).setChecked(true);
				// Update the currBodyPart.
				setCurrentBodyPart(pos);
				// Start handling UI elements.
				currBodyPart.handleRedSeekBarAndEditText(redSeekBar, redEditText);
				currBodyPart.handleGreenSeekBarAndEditText(greenSeekBar, greenEditText);
				currBodyPart.handleBlueSeekBarAndEditText(blueSeekBar, blueEditText);
				currBodyPart.handleXRotSeekBarAndEditText(xRotSeekBar, xRotEditText);
				currBodyPart.handleYRotSeekBarAndEditText(yRotSeekBar, yRotEditText);
				// Set UI elements to previous values for this body part.
				currBodyPart.setUiElements(mainLayout, isPrimaryColor);
			}

			@Override public void onNothingSelected(AdapterView<?> arg0) {}
		});
	}
	
	public static void handleShapesSpinner(final Context context, final Spinner shapes) {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
		        R.array.shapes, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		shapes.setAdapter(adapter);
		shapes.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				ViewGroup mainLayout = (ViewGroup) shapes.getParent().getParent();
				onShapeSelected(pos, mainLayout);
			}

			@Override public void onNothingSelected(AdapterView<?> arg0) {}
		});
	}
	
	public static void onShapeSelected(int shapeId, ViewGroup mainLayout) {
		LayoutInflater inflater = (LayoutInflater)
				context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mainLayout.removeViewAt(mainLayout.getChildCount() - 1);
		Shape shape = null;
		switch (shapeId) {
		case Shape.CUBE:
			View cubeParams = inflater.inflate(R.layout.cube_params, mainLayout, true);
			shape = new Cube((SeekBar) cubeParams.findViewById(R.id.cubeSize));
			break;
		 case Shape.SPHERE:
			View sphereParams = inflater.inflate(R.layout.sphere_params, mainLayout, true);
			shape = new Sphere((SeekBar) sphereParams.findViewById(R.id.sphereDiameter));
			break;
		case Shape.CONE:
			View coneParams = inflater.inflate(R.layout.cone_params, mainLayout, true);
			shape = new Cone((SeekBar) coneParams.findViewById(R.id.coneDiameter),
					(SeekBar) coneParams.findViewById(R.id.coneHeight));
			break;
		case Shape.CYLINDER:
			View cylinderParams = inflater.inflate(R.layout.cylinder_params, mainLayout, true);
			shape = new Cylinder((SeekBar) cylinderParams.findViewById(R.id.cylinderDiameter),
					(SeekBar) cylinderParams.findViewById(R.id.cylinderHeight));
			break;
		}
		currBodyPart.setShape(shape);
		currBodyPart.updateBodyPartOnScreen(); // Draw on screen.
	}
	
	public static void handlePrimaryOrSecondaryColorRadioButton(final RadioGroup pOrS) {
		pOrS.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup primaryOrSecondary, int checkedId) {
				isPrimaryColor = checkedId == R.id.primaryColor;
				final ViewGroup mainLayout = (ViewGroup) primaryOrSecondary.getParent();
				currBodyPart.setUiElements(mainLayout, isPrimaryColor);
			}
		});
	}
	
	public static void handleRedSeekBarAndEditText(final SeekBar red, final EditText redEdit) {
		// Handled in BodyPart.java.
		redSeekBar = red;
		redEditText = redEdit;
	}
	
	public static void handleGreenSeekBarAndEditText(final SeekBar green, final EditText greenEdit) {
		// Handled in BodyPart.java.
		greenSeekBar = green;
		greenEditText = greenEdit;
	}
	
	public static void handleBlueSeekBarAndEditText(final SeekBar blue, final EditText blueEdit) {
		// Handled in BodyPart.java.
		blueSeekBar = blue;
		blueEditText = blueEdit;
	}
	
	public static void handleXRotSeekBarAndEditText(final SeekBar xRot, final EditText xRotEdit) {
		// Handled in BodyPart.java.
		xRotSeekBar = xRot;
		xRotEditText = xRotEdit;
	}
	
	public static void handleYRotSeekBarAndEditText(final SeekBar yRot, final EditText yRotEdit) {
		// Handled in BodyPart.java.
		yRotSeekBar = yRot;
		yRotEditText = yRotEdit;
	}
	
	private static void setCurrentBodyPart(int bodyPart) {
		switch (bodyPart) {
		case BodyPart.HEAD: currBodyPart = Character.HEAD; break;
		case BodyPart.ARM: currBodyPart = Character.ARM; break;
		case BodyPart.LEG: currBodyPart = Character.LEG; break;
		case BodyPart.TORSO: default: currBodyPart = Character.TORSO;
		}
	}
}
