package com.wickhambros.charactercreator.Utils;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.wickhambros.charactercreator.Shapes.Character;
import com.wickhambros.charactercreator.Shapes.Shape;

public class CharacterExporterUtils {
	
	public static final float CONE_CYLINDER_X_ROTATION_CORRECTION = 90.0f;

	private static final String CHARACTER_TXT_HEADER =
			"# This file specifies the shapes that makes up the character's body parts.\n" +
			"# Each line must be formatted as follows: <body-part> <shape> <primary-color>" +
			" <secondary-color> <x-Rotation> <y-Rotation> <other-params>\n" +
			"# <body-part> may be one of TORSO, HEAD, ARM, or LEG.\n" +
			"# <shape> may be one of SPHERE, CUBE, CONE, or CYLINDER.\n" +
			"# <primary-color> and <secondary-color> must be 3 floating point values separated" +
			" by spaces, representing RGB values (e.g. 1.0 0.0 0.0 for red).\n" +
			"# <x-Rotation> and <y-Rotation> must be floating point values, representing the" +
			" degrees of rotation about the x- and y-axes, respectively.\n" +
			"# <other-params> depends on what shape is being used:\n" +
			"#\tSPHERE: <radius>\n" +
			"#\tCUBE: <size>\n" +
			"#\tCONE: <radius> <height>\n" +
			"#\tCYLINDER: <radius> <height>\n" +
			"# Finally, the line \"DISTANCE_BETWEEN_PARTS <distance>\" specifies how much space" +
			" should be put between parts when the character is drawn.\n" +
			"# Empty lines and lines starting with '#' will be ignored.\n\n\n";

	private static final String DISTANCE_BETWEEN_PARTS = "DISTANCE_BETWEEN_PARTS";
	private static final String TORSO= "TORSO";
	private static final String HEAD = "HEAD";
	private static final String ARM = "ARM";
	private static final String LEG = "LEG";
	
	private static final String[] SHAPES = {"CUBE", "SPHERE", "CONE", "CYLINDER"};
	
	public static void generateAndExportCharacterTxt(Context context, float distanceBetweenParts) {
		exportGeneratedCharacterTxt(generateCharacterTxt(distanceBetweenParts), context);
	}
	
	private static String generateCharacterTxt(float distanceBetweenParts) {
		StringBuilder builder = new StringBuilder(CHARACTER_TXT_HEADER);
		builder.append(DISTANCE_BETWEEN_PARTS)
			.append(" ").append(distanceBetweenParts).append("\n");
		builder.append(TORSO).append(" ").append(SHAPES[Character.TORSO.shape.getShapeId()]);
		appendShapeValuesToStringBuilder(builder, Character.TORSO.shape.getValues());
		builder.append(HEAD).append(" ").append(SHAPES[Character.HEAD.shape.getShapeId()]);;
		appendShapeValuesToStringBuilder(builder, Character.HEAD.shape.getValues());
		builder.append(ARM).append(" ").append(SHAPES[Character.ARM.shape.getShapeId()]);;
		appendShapeValuesToStringBuilder(builder, Character.ARM.shape.getValues());
		builder.append(LEG).append(" ").append(SHAPES[Character.LEG.shape.getShapeId()]);;
		appendShapeValuesToStringBuilder(builder, Character.LEG.shape.getValues());
		return builder.toString();
	}
	
	private static void appendShapeValuesToStringBuilder(StringBuilder builder,
			float[] values) {
		for (float value : values) {
			builder.append(" ").append(value);
		}
		builder.append("\n");
	}

	private static void exportGeneratedCharacterTxt(String characterTxt, Context context) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT, "character.txt");
		i.putExtra(Intent.EXTRA_TEXT, characterTxt);
		try {
		    context.startActivity(Intent.createChooser(i, "Send character.txt contents..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(context, "No apps were found that could send the text.",
		    		Toast.LENGTH_SHORT).show();
		}
	}
}
