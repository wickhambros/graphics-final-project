package com.wickhambros.charactercreator.Utils;

import com.wickhambros.charactercreator.OpenGlJniWrapper;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class RotationUtils {
	private static volatile float mDeltaAngleX; // Angle of rotation around x-axis.
	private static volatile float mDeltaAngleY; // Angle of rotation around y-axis.
	private static float mPreviousX;
	private static float mPreviousY;
	
	/** Returns an OnTouchListener that updates the character's rotation when user swipes around. */
	public static OnTouchListener glSurfaceViewRotationTouchListener(Activity activity) {
		final DisplayMetrics displayMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		final float density = displayMetrics.density;
		return new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				if (event != null) {
			        float x = event.getX();
			        float y = event.getY();
			 
			        if (event.getAction() == MotionEvent.ACTION_MOVE) {
			            float deltaX = (x - mPreviousX) / density / 2f;
			            float deltaY = (y - mPreviousY) / density / 2f;
			            mDeltaAngleY += deltaX; // Swiping left to right rotates around y-axis.
			            mDeltaAngleX += deltaY; // Swiping down to up rotates around x-axis.
			        }
			 
			        mPreviousX = x;
			        mPreviousY = y;
			    }
				return true;
			}
		};
	}
	
	/**
	 * Accumulates the rotations (mDeltaAngleX and Y) and sends the rotations for this frame to
	 * the native OpenGL code to multiply it by the accumulated rotation matrix for all frames.
	 */
	public static void accumulateRotations() {
		OpenGlJniWrapper.multiply_rotation(mDeltaAngleX, mDeltaAngleY);
		mDeltaAngleX = mDeltaAngleY = 0;
	}
}
