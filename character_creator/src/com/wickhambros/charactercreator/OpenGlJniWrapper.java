package com.wickhambros.charactercreator;

/** 
 * Defines the native CPP functions that we want to be able to call from Java.
 * Run "generate_jni_header.bat" to generate the corresponding header file under jni/.
 * Alternatively, run javah -classpath bin/classes -d jni com.wickhambros.charactercreator.OpenGlJniWrapper
 */
public class OpenGlJniWrapper {
	static {
        System.loadLibrary("opengl");
    }

	// GLSurfaceView.Renderer functions.
    public static native void on_surface_created();
    public static native void on_surface_changed(int width, int height);
    public static native void on_draw_frame();
    
    // Character transformation functions.
    public static native void multiply_rotation(float angleX, float angleY);
    public static native void set_dist_between_parts(double dist_between_parts);
    
    public static native void update_body_part(int bodyPart, int shape,
    		float pR, float pG, float pB, float sR, float sG, float sB,
    		float xR, float yR, float[] otherParams);
}
