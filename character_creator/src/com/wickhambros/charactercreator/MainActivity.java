package com.wickhambros.charactercreator;

import android.content.res.Configuration;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.wickhambros.charactercreator.Utils.CharacterExporterUtils;
import com.wickhambros.charactercreator.Utils.RotationUtils;
import com.wickhambros.charactercreator.Utils.UserInterfaceUtils;

public class MainActivity extends ActionBarActivity {

	private GLSurfaceView mGlSurfaceView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        mGlSurfaceView = (GLSurfaceView) findViewById(R.id.glSurfaceView);
        mGlSurfaceView.setEGLContextClientVersion(1);
        final Renderer renderer = new RendererWrapper();
        mGlSurfaceView.setRenderer(renderer);
        mGlSurfaceView.setOnTouchListener(RotationUtils.glSurfaceViewRotationTouchListener(this));
        
        final SeekBar distBetweenParts = (SeekBar) findViewById(R.id.distBetweenParts);
        UserInterfaceUtils.handleDistBetweenPartsSeekBar(distBetweenParts);
        
        final Spinner bodyPartsSpinner = (Spinner) findViewById(R.id.bodyPartsSpinner);
        UserInterfaceUtils.handleBodyPartsSpinner(this, bodyPartsSpinner);
        
        final Spinner shapesSpinner = (Spinner) findViewById(R.id.shapesSpinner);
        UserInterfaceUtils.handleShapesSpinner(this, shapesSpinner);
        
        final RadioGroup primaryOrSecondary =
        		(RadioGroup) findViewById(R.id.primaryOrSecondaryColor);
        UserInterfaceUtils.handlePrimaryOrSecondaryColorRadioButton(primaryOrSecondary);
        
        final SeekBar red = (SeekBar) findViewById(R.id.red);
        final EditText redEdit = (EditText) findViewById(R.id.redEdit);
        UserInterfaceUtils.handleRedSeekBarAndEditText(red, redEdit);
        
        final SeekBar blue = (SeekBar) findViewById(R.id.blue);
        final EditText blueEdit = (EditText) findViewById(R.id.blueEdit);
        UserInterfaceUtils.handleBlueSeekBarAndEditText(blue, blueEdit);
        
        final SeekBar green = (SeekBar) findViewById(R.id.green);
        final EditText greenEdit = (EditText) findViewById(R.id.greenEdit);
        UserInterfaceUtils.handleGreenSeekBarAndEditText(green, greenEdit);
        
        final SeekBar xRot = (SeekBar) findViewById(R.id.xRot);
        final EditText xRotEdit = (EditText) findViewById(R.id.xRotEdit);
        UserInterfaceUtils.handleXRotSeekBarAndEditText(xRot, xRotEdit);
        
        final SeekBar yRot = (SeekBar) findViewById(R.id.yRot);
        final EditText yRotEdit = (EditText) findViewById(R.id.yRotEdit);
        UserInterfaceUtils.handleYRotSeekBarAndEditText(yRot, yRotEdit);
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	 
	    mGlSurfaceView.onPause();
	}
	 
	@Override
	protected void onResume() {
	    super.onResume();
	 
	    mGlSurfaceView.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.export_character) {
			SeekBar distanceBetweenPartsSeekBar = (SeekBar) findViewById(R.id.distBetweenParts);
			float distanceBetweenParts = distanceBetweenPartsSeekBar.getProgress() / 100.0f;
			CharacterExporterUtils.generateAndExportCharacterTxt(this, distanceBetweenParts);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
