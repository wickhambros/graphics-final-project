#ifdef __APPLE__  // include Mac OS X versions of headers
#  include <OpenGL/OpenGL.h>
#  include <GLUT/glut.h>
#else // non-Mac OS X operating systems
#  include <GL/glew.h>
#  include <GL/freeglut.h>
#  include <GL/freeglut_ext.h>
#endif  // __APPLE__

#include <iostream>
#include <math.h>
#include <sys/time.h>
#include "3DShapes.h"
#include "drawCharacter.h"
#include "characterLoader.h"

Shape *torso, *head, *arm, *leg;
bool initialized = false;
float distanceBetweenParts;
float animationDistance;
float animationStep;

float currAnimDist = 0.0;
bool animReversed = false;
bool reversingAnimation = false;
bool animationStopped = false;
double previousTime;

using namespace std;

double getCurrentTime() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return tp.tv_sec + tp.tv_usec * 1.0e-6;
}

void initCharacterShapes(void) {
    loadCharacterFromFile("character.txt", torso, head, arm, leg, distanceBetweenParts);
    animationDistance = 0.7 * distanceBetweenParts;
    animationStep = animationDistance * 7;
    previousTime = getCurrentTime();
}

void drawCharacter(void) {
    if (!initialized) {
        initCharacterShapes();
        initialized = true;
    }
    glPushMatrix();
        glRotatef(currAnimDist * 50, 0, 1, 0);
        torso->draw();
        glPushMatrix();
            float distToTopOfTorso = torso->height() / 2 + head->height() / 2;
            glTranslatef(0, distToTopOfTorso + distanceBetweenParts, 0);
            glRotatef(currAnimDist * 50, 0, 0, 1);
            head->draw();
        glPopMatrix();
        float distToSideOfTorso = torso->width() / 2 + arm->width() / 2;
        glPushMatrix();
            glTranslatef(distToSideOfTorso + distanceBetweenParts, 0,
                -currAnimDist);
            arm->draw();
        glPopMatrix();
        glPushMatrix();
            glTranslatef(-distToSideOfTorso - distanceBetweenParts, 0,
                currAnimDist);
            arm->draw();
        glPopMatrix();
        float distToBottomOfTorso = torso->height() / 2 + leg->height() / 2;
        glPushMatrix();
            glTranslatef(-(distanceBetweenParts + leg->height()) / 2,
                -distToBottomOfTorso - distanceBetweenParts - currAnimDist,
                0);
            leg->draw();
        glPopMatrix();
        glPushMatrix();
            glTranslatef((distanceBetweenParts + leg->height()) / 2,
                -distToBottomOfTorso - distanceBetweenParts + currAnimDist,
                0);
            leg->draw();
        glPopMatrix();
    glPopMatrix();
}

void animate(void) {
    // Always show walking animation.
    double currentTime = getCurrentTime();
    double dTime;
    if (animationStopped) {
        dTime = 0;
        currAnimDist = 0;
        animationStopped = false;
    } else {
        dTime = currentTime - previousTime;
    }
    previousTime = currentTime;
    currAnimDist += (animReversed ? -animationStep : animationStep) * dTime;
    if (fabs(currAnimDist) > animationDistance) {
        if (!reversingAnimation) {
            animReversed = !animReversed;
            reversingAnimation = true;
        }
    } else {
        reversingAnimation = false;
    }
    glutPostRedisplay();
}

void stopAnimation() {
    currAnimDist = 0;
    animationStopped = true;
    animate();
}
