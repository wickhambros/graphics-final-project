#ifndef DRAW_CHAR_H
#define DRAW_CHAR_H

#include "3DShapes.h"

void drawCharacter(void);
void animate(void);
void stopAnimation(void);

#endif