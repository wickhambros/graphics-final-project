#ifdef __APPLE__  // include Mac OS X versions of headers
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else // non-Mac OS X operating systems
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>
#endif  // __APPLE__

#include "drawCharacter.h"
#include "Vec3.h"

using std::cout;
using std::endl;

// Parameters for the camera perspective.
static Vec3 cameraCenter(0.0, 1.0, -2.0);
static Vec3 characterCenter(-1.0, 0.0, 0.0);
static Vec3 up(0.0, 0.0, 1.0);
static double theta = 0.0;  // Degrees.
static const double PI = 3.1415926f;
static const double TWO_PI = 2.0 * PI;
static const double DEG_TO_RAD = PI / 180.0;

static void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(cameraCenter.x, cameraCenter.y, cameraCenter.z,
              0, 0, 0,
              up.x, up.y, up.z);
    drawCharacter();
    glRotatef(theta, 0, 1, 0);
    glTranslatef(-characterCenter.x, characterCenter.y, -characterCenter.z);
    glPushMatrix();
        glutSolidSphere(0.25, 15, 15);
    glPopMatrix();
    glutSwapBuffers();
}

static void setProjection(double w, double h) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, w / h, 0.1, 30.0);
}

static void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    setProjection(w, h);
}

/**
 * Allow the user to exit the program by pressing Esc or the q key.
 */
static void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        // Escape or q exits the program.
        case 033: // Esc.
        case 'q':
            exit(EXIT_SUCCESS);
            break;
    }
}

static void arrowKeys(int key, int x, int y) {
    switch (key) {
        case 101:  // up.
            characterCenter +=
                    Vec3(-0.1*sin(theta*DEG_TO_RAD), 0, 0.1*cos(theta*DEG_TO_RAD));
            break;
        case 100:  // left.
            theta -= 2;
            break;
        case 103:  // down.
            characterCenter -=
                    Vec3(-0.1*sin(theta*DEG_TO_RAD), 0, 0.1*cos(theta*DEG_TO_RAD));
            break;
        case 102:  //right.
            theta += 2;
            break;
    }
    if (theta >= 360) {
        theta -= 360;
    }
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(512, 512);
    glutCreateWindow("Scene");

    glClearColor(0.3, 0.8, 0.2, 1.0);
    glEnable(GL_DEPTH_TEST);

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(arrowKeys);
    glutIdleFunc(animate);
//    stopAnimation();

    setProjection(512, 512);

    glutMainLoop();
    return 0;
}
